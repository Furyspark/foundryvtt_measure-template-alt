export function initSettings() {
  game.settings.register("measure-templates-alt", "enableModule", {
    name: "MeasureTemplateAlt.SETTINGS.enableModuleN",
    hint: "MeasureTemplateAlt.SETTINGS.enableModuleH",
    scope: "world",
    config: true,
    default: true,
    type: Boolean,
  });
}