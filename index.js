import "./src/measure.js";
import {initSettings} from "./src/settings.js";

Hooks.once("init", () => {
  initSettings();
});